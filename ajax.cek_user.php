<?php

include("app.init.php");

if ($_SESSION['login'] || !$_POST) {
    header("location:index.php");
    exit();
}

if (isset($_POST['var_usn']) AND isset($_POST['var_pwd'])) {
    $db = connectdb();

    $username = $db->real_escape_string($_POST['var_usn']);
    $password = $db->real_escape_string($_POST['var_pwd']);

    $result = $db->query("SELECT user_id,level,nama FROM tbl_users "
        . "WHERE user_id='$username' AND password='$password' ")
        or die($_DEV_MODE ? var_dump(get_defined_vars()) : "mysql #$db->errno");

    if ($result and $result->num_rows > 0) {
        if ($row = $result->fetch_assoc()) {
            $_SESSION['login']['level'] = $row['level'];
            $_SESSION['login']['nama']  = $row['nama'];
            //$_SESSION['login']['pwd']   = $password;

            die("OK");
        }
    } else {
        die("Username atau password salah!");
    }
}

$_DEV_MODE && var_dump(get_defined_vars());
