<?php
include('app.init.php');

//koneksi database
$db = connectdb();

// variabel pagination
$limit = 20;
$page  = ($_GET['p'] < 1) ? 1 : intval($_GET['p']);
$start = ($page - 1) * $limit;

if (isset($_GET['cari'])) { // proses pencarian
    $q     = $db->real_escape_string($_GET['cari']);
    // query no_hp atau nama supir
    $tabel = "tbl_hp WHERE no_hp LIKE '%$q%' OR nm_supir LIKE '%$q%' ORDER BY no_hp";

    if ($res_total = $db->query("SELECT count(*) FROM $tabel") or die($db->error)) {
        $total  = $res_total->fetch_array()[0];
        $result = $db->query("SELECT * FROM $tabel LIMIT $start,$limit") or die($db->error);
        $rows   = $result ? $result->fetch_all(MYSQLI_ASSOC) : [];
    }
} else {
    $result = $db->query("SELECT * FROM tbl_hp ORDER BY no_hp") or die($db->error);

    if ($result) {
        $total = $result->num_rows;
        $rows  = $result->fetch_all(MYSQLI_ASSOC);
        $rows  = array_slice($rows, $start, $limit);
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            <!--
            .style2 {color: #000000}
            .style3 {color: #0000CC}
            .style5 {color: #3399FF}
            .style6 {font-family: "Times New Roman", Times, serif}
            body {
                background-color: #00CCFF;
                background-image: url(gambar%20pendukung/back.png);
            }
            -->
        </style>
        <title>Sistem Informasi Distribusi</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" type="text/css" href="css/easyui/metro-green.css">
        <link rel="stylesheet" type="text/css" href="css/easyui/icon.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.easyui.min.js"></script>
        <script src="js/app.proses-hp.js"></script>
    </head>

    <body>
        <nav>
            <div align="center">
                <table width="901" height="764" border="">
                    <tr>
                        <th width="50" height="88" colspan="0" align="center" bgcolor="#3366FF" scope="col"><div align="center"><span class="style5"><img src="../logo serim.png" width="421" height="48"></span><img src="gambar pendukung/banner.png" width="880" height="242"></div></th>
                    </tr>

                    <tr>
                        <th width="50" height="50" colspan="3" scope="col">
                            <?php include "menu.php"; ?>
                        </th>
                    </tr>

                    <tr>
                        <td colspan="0" bgcolor="#FFFFFF">
                            <form>
                                <table width="180" border="0" align="right">
                                    <tr>
                                        <th width="52" scope="col">Cari</th>
                                        <th width="8" scope="col">:</th>

                                        <th><input type="text" name="cari" class="" placeholder="No HP atau nama"/></th>
                                        <th><input type="submit" value="Cari"/></th>
                                        <th><input type="button" onclick="location = ('master_hp.php')" value="Refresh"/></th>
                                    </tr>
                                </table>
                            </form>
                            <h1 align="center" class="style5">DATA HP SUPIR </h1>
                            <table width="550" border="1" align="center">
                                <tr bgcolor="#006699">
                                    <th bgcolor="#0099FF"><div align="center"><span class="style2">No.</span></div></th>
                                <th bgcolor="#0099FF"><div align="center">Nomor HP </div></th>
                                <th bgcolor="#0099FF"><div align="center"><span class="style2">Nama Supir </span></div></th>
                                <th bgcolor="#0099FF"><div align="center"><span class="style2">Opsi</span></div></th>
                    </tr>
                    <?php
                    if (count($rows) > 0):
                        foreach ($rows as $data): //perulangan while dg membuat variabel $data yang akan mengambil data di database
                            extract($data);
                            ?>
                            <tr>
                                <td bordercolor="#FFFFFF"><?php echo ++$start ?></td>
                                <td bordercolor="#FFFFFF"><?php echo $no_hp ?></td>
                                <td bordercolor="#FFFFFF"><?php echo $nm_supir ?></td>

                                <td bordercolor="#FFFFFF">
                                    <a class="l-btn l-btn-small" onclick="showFormEditHP('<?php echo "{$no_hp}','{$nm_supir}" ?>')">Edit</a>
                                    <a class="l-btn l-btn-small" onclick="confirmHapusHP('<?php echo $no_hp ?>')">Hapus</a>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        echo '<tr><td align="center" colspan="4">' . pagination_links($page, $total, $limit) . '</td></tr>';
                    else:
                        echo '<tr><td height="50" colspan="4">Data tidak ditemukan</td></tr>';
                    endif;
                    ?>
                </table>
                <p align="left" class="style5">&nbsp;</p>
                <p align="center"><br/></p>
                <div align="center"></div>
                </td>
                </tr>

                <tr>
                    <td height="88" colspan="3"><img src="gambar pendukung/fooooter.jpg" width="891" height="81"></td>
                </tr>
                </table>
            </div>
        </nav>
        <div>
            <div>
                <div align="center">
                    <p><a href="https://www.facebook.com/pages/PT-Serim-Indonesia/1002299839810166" target="_blank"> . </a></p>
                </div>
            </div>
            <div></div>
        </div>
        <p align="center"></p>

        <h2 class="style3">&nbsp; </h2>
        <h2 align="center" class="style3">&nbsp;</h2>

        <link href="css/style.css" rel="stylesheet" />
        <div align="center"></div>
    </body>
</html>
