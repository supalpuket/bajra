!(window.$) && alert('Error: jQuery not loaded!');

$(function () {
    $('#txt_username').focus();

    $("#login_form").submit(function () {

        //Mengambil value dari input username & Password
        var username = $('#txt_username').val();
        var password = $('#txt_password').val();

        //Ubah alamat url berikut, sesuaikan dengan alamat script pada komputer anda
        var url_login = 'ajax.cek_user.php';
        var url_admin = 'index.php';

        $.ajax({
            url: url_login,
            //mengirimkan username dan password ke script login.php
            data: 'var_usn=' + username + '&var_pwd=' + password,
            //Method pengiriman
            type: 'POST',
            //Data yang akan diambil dari script pemroses
            dataType: 'html',
            //Respon jika data berhasil dikirim
            success: function (pesan) {
                console.log('Login: ' + pesan);

                if (pesan == 'OK') {
                    //Arahkan ke halaman admin jika script pemroses mencetak kata ok
                    window.location = url_admin;
                } else {
                    //Cetak peringatan untuk username & password salah
                    alert(pesan);
                }
            }
        });

        return false;
    });

    window.alert = function (o) {
        var div = $("#login_form").children().get(0);

        if (o && o.close === true) {
            div.style.display = 'none';
            $('#txt_username').focus();
            return;
        }

        if (div.className != 'alert alert-warning') {
            div = $('<div>').get(0);
            div.className = 'alert alert-warning';
            $("#login_form").prepend($(div));
        }

        div.innerHTML = '<a href="#" class="close" onclick="alert({close:true})">&times;</a>' + o;
        div.style.display = 'block';
    }

});
