!(window.$ && $.fn.window) && alert('Error: jQuery or jQuery EasyUI not loaded!');

var formHP;
var ajax_url = 'ajax.proses_hp.php';

$(function () {
    // form tambah hp
    if (formHP = $('#tambah-hp')[0]) {
        formHP.no_hp.focus();
        formHP.onsubmit = tambahHP;
    }
});

// form edit hp
function getFormEditHP() {
    if (!formHP) {
        formHP = $('<form>')[0];
        formHP.innerHTML = ('\
        <table style="border-spacing:10px"><tr>\
            <td><label for="no_hp">No HP </label></td>\
            <td>: <input class="easyui-validatebox" name="no_hp" disabled/></td>\
        </tr><tr style="padding-top:10px">\n\
            <td><label for="nm_supir">Nama Supir </label></td>\
            <td>: <input class="easyui-validatebox" name="nm_supir" required/></td>\
        </tr><tr>\n\
        <td colspan="2" style="text-align:center">\
            <input type="submit" value="Simpan">\
        </td></tr></table>\
        ');

        formHP.onsubmit = editHP; // submit handler
    }

    return formHP;
}

// menampilkan form edit hp
function  showFormEditHP(no_hp, nm_supir) {
    $(formHP = getFormEditHP()).window({title: 'Edit HP'});
    formHP.no_hp.value = no_hp;
    formHP.nm_supir.value = nm_supir;
    formHP.nm_supir.focus();
}


// dialog hapus
function confirmHapusHP(no_hp) {
    $.messager.confirm('Konfirmasi',
            'Hapus data hp dari database?',
            function (_ok_) {
                if (_ok_) {
                    hapusHP(no_hp);
                }
            }
    );
}


// Tambah HP via ajax saat form dikirim "on submission"
function tambahHP() {
    var no_hp = formHP.no_hp.value;
    var nm_supir = formHP.nm_supir.value;

    $.ajax({url: ajax_url + '?nama=tambah&no_hp=' + no_hp + '&nm_supir=' + nm_supir,
        success: function (data) {
            if (data == 'OK') {
                $.messager.alert('Info', 'Data HP berhasil ditambahkan', 'info',
                        function () {
                            formHP.nm_supir.value = '';
                            formHP.no_hp.value = '';
                            formHP.no_hp.focus();
                        });
            } else {
                $.messager.alert('Gagal', data, 'error',
                        function () {
                            formHP.no_hp.select();
                            formHP.no_hp.focus();
                        });
            }
        }
    });

    return false;
}

// Fungsi edit data HP via ajax
function editHP() {
    var no_hp = formHP.no_hp.value;
    var nm_supir = formHP.nm_supir.value;

    $.ajax({url: ajax_url + '?nama=edit&no_hp=' + no_hp + '&nm_supir=' + nm_supir,
        success: function (data) {
            if (data == 'OK') {
                location.reload();
            } else {
                console.log(data);
                $.messager.alert('Error', data, 'error');
            }
            $(formHP).window('close');
        }
    });

    return false;
}

// hapus data HP via ajax
function hapusHP(no_hp) {
    $.ajax({url: ajax_url + '?nama=hapus&no_hp=' + no_hp,
        success: function (data) {
            if (data == 'OK') {
                location.reload();
            } else {
                $.messager.alert('Gagal menghapus data', data, 'error');
            }
        }
    });
}
