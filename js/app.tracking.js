!(window.$) && alert('Error: jQuery not loaded!');

$(function () {

    $('#daftar_sj').change(function () {

        // not yet implemented
        return;

        $.ajax({
            url: '../ajax.cek_sj.php?no_sj=' + this.value,
            data: '',
            success: function () {
            }
        });
    });

    showMap();
});


var script = document.createElement('script');
script.src = "https://maps.googleapis.com/maps/api/js";
document.getElementsByTagName('head')[0].appendChild(script);

var map; // Global declaration of the map
var latlng = new google.maps.LatLng(-6.229465, 107.001759);

function showMap()
{
    var myOptions = {
        zoom: 13,
        center: new google.maps.LatLng(-6.229465, 107.001759),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    var markerOptions = {
        map: map,
        position: new google.maps.LatLng(-6.229465, 107.001759)
    };
    marker = new google.maps.Marker(markerOptions);

    google.maps.event.addListener(marker, "click", function ()
    {
        iw.setContent("This is an infowindow");
        iw.open(map, this);
    });
}
