<?php
include("../app.init.php");

$auth_info = "(Guest)";

if ($auth = $_SESSION['login']) {
    $auth_info = "$auth[nama] ($auth[level])";
}

//koneksi database
$db = connectdb();

if ($_GET['no_sj']) {
    $no_sj = $db->real_escape_string(strtoupper($_GET['no_sj']));

    $sql = "SELECT * FROM tbl_suratjalan AS sj "
        ."JOIN tracking ON sj.no_hp=tracking.no_hp "
        ."JOIN tbl_jadwal ON sj.no_suratjalan=tbl_jadwal.no_suratjalan "
        ."WHERE sj.no_suratjalan='$no_sj' "
    ;
    $result = $db->query($sql);

    if ($result->num_rows > 0) {
        extract($row = $result->fetch_assoc());
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            <!--
            .style2 {color: #000000}
            .style3 {color: #0000CC}
            .style5 {color: #3399FF}
            .style6 {font-family: "Times New Roman", Times, serif}
            body {
                background-color: #00CCFF;
                background-image: url(../gambar%20pendukung/back.png);
            }
            -->
        </style>
        <title>Tracking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="../css/easyui/metro-green.css">
        <link rel="stylesheet" type="text/css" href="../css/easyui/icon.css">
    </head>

    <body>
        <nav>
            <div align="center">
                <table width="901" height="764" border="">
                    <tr>
                        <th width="50" height="88" colspan="0" align="center" bgcolor="#3366FF" scope="col"><div align="center"><span class="style5"><img src="../logo serim.png" width="421" height="48"></span><img src="gambar pendukung/banner.png" width="880" height="242"></div></th>
                    </tr>

                    <tr>
                        <th width="50" height="50" colspan="3" scope="col">
                    <div id='cssmenu'>
                        <ul>
                            <li class='active has-sub'><a href='../index.php'>BERANDA</a></li>
                            <li class='active has-sub'><a href='#'>About Me </a></li>
                            <li class='active has-sub'>
                                <?php echo empty($_SESSION['login']) ? "<a href='../login.php'>LOG IN</a>" : "<a href='../keluar.php'>LOG OUT</a>"; ?>
                            </li>
                        </ul>
                    </div>
                    </th>
                    </tr>

                    <tr>
                        <td colspan="0" bgcolor="#FFFFFF">
                            <div style="overflow: auto">
                                <h4 style="float: right;margin: 10px"><?php echo $auth_info ?></h4>
                            </div>

                            <h1 align="center" class="style5">TRACKING</h1>
                            <div style="margin:auto;width:75%">
                                <form id="cari_sj">
                                    <table><tr>
                                            <td style="width:75%">
                                                <label for="no_sj">No. Surat Jalan</label><br/>
                                                <input id="no_sj" name="no_sj" placeholder="SJ0000-00000"/>
                                                <input type="submit" value="Cari"/>
                                            </td>
                                            <?php
                                            if (!empty($suratjalans)):
                                                echo "<td align><label for='daftar_sj'>Pilih Surat Jalan</label><br/>";
                                                echo "<select id='daftar_sj'>";

                                                foreach ($suratjalans as $no_sj):
                                                    echo "<option value='$no_sj'>$no_sj</option>";
                                                endforeach;

                                                echo "</select></td>";
                                            endif;
                                            ?>
                                        </tr></table>
                                </form><br/>

                                <?php if ($no_suratjalan): ?>
                                    <table width="100%" border class="style6 style3">
                                        <tr><th colspan="5"><h4>DATA SURAT JALAN</h4></th></tr>
                                        <tr><td width="120px">No. Surat Jalan  </td><td><?php echo $no_suratjalan ?><br/>   </td></tr>
                                        <tr><td width="120px">Tanggal          </td><td><?php echo $tanggal ?><br/></td></tr>
                                        <tr><td width="120px">Customer         </td><td><?php echo $customer ?><br/></td></tr>
                                        <tr><td width="120px">Perusahaan       </td><td><?php echo $nm_perusahaan ?><br/></td></tr>
                                        <tr><td width="120px">Alamat           </td><td><?php echo $alamat ?><br/></td></tr>
                                        <tr><td width="120px">No. Kendaraan    </td><td><?php echo $no_kendaraan ?><br/></td></tr>
                                        <tr><th colspan="5"><h4>SHIPMENT</h4></th></tr>
                                        <tr><td width="120px">Status           </td><td><?php echo $status ?><br/></td></tr>
                                        <tr><td width="120px">Posisi           </td><td><?php echo $nama_tempat ?><br/></td></tr>
                                        <tr><td width="120px">Updated          </td><td><?php echo $updated ?><br/> </td></tr>
                                    </table>
                                    <div id="map" style="margin-top: 20px;border: 1px solid;height: 600px"></div>
                                <?php endif ?>
                            </div>
                            <p align="left" class="style5">&nbsp;</p>
                            <p align="center"><br/></p>
                            <div align="center"></div>
                        </td>
                    </tr>

                    <tr>
                        <td height="88" colspan="3"><img src="gambar pendukung/fooooter.jpg" width="891" height="81"></td>
                    </tr>
                </table>
            </div>
        </nav>
        <div>
            <div>
                <div align="center">
                    <p><a href="https://www.facebook.com/pages/PT-Serim-Indonesia/1002299839810166" target="_blank"> . </a></p>
                </div>
            </div>
            <div></div>
        </div>
        <p align = "center"></p>
        <h2 class = "style3">&nbsp;
        </h2>
        <h2 align = "center" class = "style3">&nbsp;
        </h2>
        <div align = "center"></div>
    </body>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.easyui.min.js"></script>
    <script src="../js/app.tracking.js"></script>
</html>
