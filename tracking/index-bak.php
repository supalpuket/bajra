<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="themes/icon.css">
	<link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css"/>
	<link rel="stylesheet" href="jqwidgets/styles/jqx.web.css" type="text/css" />
	<link rel="stylesheet" href="jqwidgets/styles/jqx.ui-sunny.css" type="text/css" />
	<link rel="stylesheet" href="jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
	<script type="text/javascript" src="scripts/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxwindow.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxradiobutton.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>	
	<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>	
	<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxpanel.js"></script>
    <script type="text/javascript" src="jqwidgets/jqxtree.js"></script>
	<script type="text/javascript" src="jqwidgets/jqxcheckbox.js"></script>
	<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript" src="gmaps.js"></script>
	
    <title>MAP TRACKING AREA</title>
	
	<style type='text/css'>
		#map_container{ 
		  width: 84%;
		  height: 100%;
		  float: right;
		  border: 1px solid darkgrey;
		}
		
		#map_container_kiri{ 
		  width: 15%;
		  height: 54%;
		  float: left;
		  border: 1px solid darkgrey;
		}
		
		#map_canvas2{
		  padding:5px 5px 5px 5px;
		  float: left;
		}
		
		#map_canvas3{
		  padding:5px 5px 5px 5px;
		}
		
		#map_canvas{
		  width: 99%;
		  height: 82%;
		  margin-left:5px;
		  border: 1px solid darkgrey;
		}
		#map_container_kanan{ 
		padding:5px 5px 5px 5px;
		  width: 88%;
		  height: 97%;
		  margin-right:5px;
		  margin-left:5px;
		  margin-top:5px;
		  border: 1px solid darkgrey;
		}
		
		#map_container_photo{ 
		  width: 18%;
		  height: 13%;
		  padding:5px 5px 5px 5px;
		  margin-right:5px;
		  margin-top:5px;
		  float: right;
		  border: 1px solid darkgrey;
		}
		
		input[type="text"] {
		  outline: none;
		  display: block;
		  width: 100%;
		  margin: 0 0 10px;
		  padding: 5px 10px;
		  border: 1px solid #ccc;
		  color: #ccc;
		  
		  -webkit-box-sizing: border-box;
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  font-size: 14px;
		  font-wieght: 400;
		  -webkit-font-smoothing: antialiased;
		  -moz-osx-font-smoothing: grayscale;
		  -webkit-transition: 0.2s linear;
		  -moz-transition: 0.2s linear;
		  -ms-transition: 0.2s linear;
		  -o-transition: 0.2s linear;
		  transition: 0.2s linear;
		}
		
	</style>

			
			
   <script type="text/javascript">
		jQuery.noConflict();
		$(document).ready(function () {
			 var theme = 'bootstrap';
			 var plg_cd;
			 
			
			
			$("#btn_cari").jqxButton({ 
				width: '150',
				theme: theme
			});
			
			$("#optPelanggan").jqxRadioButton({ width: 250, height: 25,  checked: true});
			$("#optGoodBad").jqxRadioButton({ width: 250, height: 25});
			$("#optKubikasi").jqxRadioButton({ width: 250, height: 25});
			
			var pilihan="00";
			
			$("#optPelanggan").on('change', function (event) {
                var checked = event.args.checked;
                if (checked) {
					localStorage.setItem('pilihan',"00");
                }
              
            });
			
			$("#optGoodBad").on('change', function (event) {
                var checked = event.args.checked;
                if (checked) {
					localStorage.setItem('pilihan',"01");
                }
              
            });
			
			$("#optKubikasi").on('change', function (event) {
                var checked = event.args.checked;
                if (checked) {
					localStorage.setItem('pilihan',"02");
                }
              
            });
			
			$('#foto').on('resizing', function (event) { 
				var path2 = localStorage.getItem("path");
				var lebar = $("#foto").width()-10;
				var tinggi = $("#foto").height()-37;
				$("#meteran").html('<img src="'+ path2 +'" width="' + lebar +'px" height="'+ tinggi +'px" />');
				
			}); 
			
			var source =
			{
				datatype: "json",
				type: 'POST',
				datafields: [
					{ name: 'urut'},
					{ name: 'nopel'},
					{ name: 'tabul'},
					{ name: 'awal'},
					{ name: 'akhir'},
					{ name: 'kubik'},
					{ name: 'saldo_ar'}
				],
				url: 'data_drd.php',
				cache: false
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source,
                {
					formatData: function (data) {
						data.plg_cd = plg_cd;
						return data;
					},
                }
            ); 
			
			

			function doSearch(plg_cd){
			jQuery('#dg').datagrid('load',{
				itemid: plg_cd
			});
			}

			var map;
			var options = {
			  zoom: 12,
			  center: new google.maps.LatLng(-6.229465, 107.001759),
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
			var map = new google.maps.Map(document.getElementById("map_canvas"), options);
			map = new google.maps.Map(document.getElementById("map_canvas"), options);
	
			function getLocation(){
				var pilihan = localStorage.getItem('pilihan');
				var map;
				var locations = new Array();
			
				var nopel = $("#nopel").val();
				var nama = $("#nama").val();
				var alamat = $("#alamat").val();
				var telpon = $("#telpon").val();
				
				$.ajax({
					url: 'data_pelanggan.php',
					type: "POST",
					data : "nopel="+ nopel + "&nama=" + nama + "&alamat=" + alamat + "&telpon=" + telpon + "&pilihan=" + pilihan,
					success: function(results) {
						var posts = JSON.parse(results);
						$.each(posts, function() {
							var valueToPush = new Array();
							
							
							valueToPush[0] = this.no_po;
							valueToPush[1] = this.nama_tempat;
							valueToPush[2] = this.latitude;
							valueToPush[3] = this.longitude;
							
							locations.push(valueToPush);
						});
						var options = {
							  zoom: 8, 
							  center: new google.maps.LatLng(-6.229465, 107.001759),
							  mapTypeId: google.maps.MapTypeId.ROADMAP
						};
    
						var map = new google.maps.Map(document.getElementById("map_canvas"), options);
						map = new google.maps.Map(document.getElementById("map_canvas"), options);
								
						console.log(locations.length); 
						var infowindow = new google.maps.InfoWindow();

						var marker, i;
					 
						for (i = 0; i < locations.length; i++) {  
							
							marker = new google.maps.Marker({
								position: new google.maps.LatLng(locations[i][2], locations[i][3]),
								map: map,
								icon: 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Marker-Outside-Pink-icon.png',						
							});
						 
							google.maps.event.addListener(marker, 'click', (function(marker, i) {
								return function() {
									doSearch(locations[i][0]);	
									plg_cd = locations[i][0];
									dataAdapter.dataBind();
									infowindow.setContent(	"Nomor PO   : " + locations[i][0] + "<br/>" + 
															"Alamat : " + locations[i][1] + "<br/>" + 
															"Tarif  : " + locations[i][3] );       
									
									infowindow.open(map, marker);
								}
						  })(marker, i));
						}
					}
				});
				
		}

				
		$("#btn_cari").click(function(){
			getLocation();
		}); 
		
		jQuery('#dg').datagrid({
			onClickRow: function(index,field,value){
				var row = $('#dg').datagrid('getSelected');
				var lebar=100;
				var tinggi=90;
				var tahunbulan= row.tabul;
				
				path = "../../webservice/photo_meter/" + tahunbulan +"/"+ row.nopel +".jpg";
				localStorage.setItem("path", path);
				
				
				var ada = ImageExist(path);
				if (ada==false) {
					path="silang.png";
				}
				
				var h = $("#foto").height()-37;
				var w = $("#foto").width()-10;
				
				/* ---> Cek Tanggal Foto */
				
				$.ajax({
					type: 'GET',
					url: 'cek_file.php',
					data : "path="+ path,
					dataType: "json",
					success: function(data) {
						if (data.tanggal_foto =="kosong") {
							xtanggal_foto ="[Foto Meteran]";
							path  ="silang.gif";
						}else {
							xtanggal_foto =data.tanggal_foto;
						}
					},
				});
				
				$("#meteran").html('<img src="'+ path +'" width="' + w +'px" height="'+ h +'px" />');
			}
		});
		
		
		function ImageExist(url) 
		{
		   var img = new Image();
		   img.src = url;
		   return img.height != 0;
		}
	});
	
	</script>
  </head>
  <body>
    <div id="map_container">
		<div id="map_canvas2">
			
		</div>
		<div id="map_canvas"></div>
	</div>
	<div id="map_container_kiri">
		<div id="map_container_kanan"> 
				<div id="map_canvas3">
					<center>
					<table border='0'>
						<tr>
							<td><input type="text" placeholder="Masukkan Nopel" id="nopel" autofocus></td>
						</tr>
						<tr>
							<td><input type="text" placeholder="Masukkan Nama" id="nama" autofocus></td>
						</tr>
						<tr>
							<td><input type="text" placeholder="Masukkan Alamat" id="alamat" autofocus></td>
						</tr>
						<tr>
							<td><input type="text" placeholder="Masukkan Telpon" id="telpon" autofocus></td>
						</tr>
					</table>
					
					<div style="margin:20px 0;">
						<input type="button" value="Cari Pelanggan" id='btn_cari' />
					</div>
					</center>
					
					<div style='margin-top: 10px;' id='optPelanggan'><span>Data Pelanggan</span></div>
					<div style='margin-top: 10px;' id='optGoodBad'><span>GoodBad</span></div>
					<div style='margin-top: 10px;' id='optKubikasi'><span>Kubikasi</span></div>
				</div>
					
			</div>

		
	</div>
	
  </body>
</html>