<html>
    <head>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
  
    
	<script type="text/javascript" src="gmaps.js"></script>



<script type="text/javascript">
	 jQuery(document).ready( function($){
	var map; // Global declaration of the map
	var iw = new google.maps.InfoWindow(); // Global declaration of the infowindow
	//var latlng = new google.maps.LatLng( -6.229465, 107.001759 );
	function initialize() 
	{
		var myOptions = {
	  		zoom: 13,
			center: new google.maps.LatLng(-6.229465, 107.001759),
	  		mapTypeId: google.maps.MapTypeId.ROADMAP
	  	}
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		
		var markerOptions = {
			map: map,
			position: new google.maps.LatLng(-6.229465, 107.001759)		
		};
		marker = new google.maps.Marker(markerOptions);
		
		google.maps.event.addListener(marker, "click", function()
		{
			iw.setContent("This is an infowindow");
			iw.open(map, this);
		});
	}
	 })
</script>
 </head>
    <body>
<div id="map_canvas"></div>
	
    </body>
</html>