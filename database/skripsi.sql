/*
Navicat MySQL Data Transfer

Source Server         : FarrahaSOfT
Source Server Version : 50539
Source Host           : localhost:3306
Source Database       : skripsi

Target Server Type    : MYSQL
Target Server Version : 50539
File Encoding         : 65001

Date: 2015-11-26 17:20:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_hp`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_hp`;
CREATE TABLE `tbl_hp` (
  `no_hp` varchar(12) NOT NULL,
  `nm_supir` char(15) NOT NULL,
  PRIMARY KEY (`no_hp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_hp
-- ----------------------------
INSERT INTO `tbl_hp` VALUES ('02170794658', 'FAKIH');
INSERT INTO `tbl_hp` VALUES ('087872285060', 'IRYAN');

-- ----------------------------
-- Table structure for `tbl_jadwal`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_jadwal`;
CREATE TABLE `tbl_jadwal` (
  `id_jadwal` varchar(6) NOT NULL,
  `tanggal` date NOT NULL,
  `no_suratjalan` varchar(12) NOT NULL,
  PRIMARY KEY (`id_jadwal`),
  KEY `no_suratjalan` (`no_suratjalan`),
  CONSTRAINT `tbl_jadwal_ibfk_1` FOREIGN KEY (`no_suratjalan`) REFERENCES `tbl_suratjalan` (`no_suratjalan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_jadwal
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_suratjalan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_suratjalan`;
CREATE TABLE `tbl_suratjalan` (
  `no_suratjalan` varchar(12) NOT NULL,
  `no_po` varchar(15) NOT NULL,
  `no_kendaraan` varchar(10) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `nm_perusahaan` text NOT NULL,
  `alamat` text NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`no_suratjalan`),
  UNIQUE KEY `no_suratjalan` (`no_suratjalan`),
  KEY `no_hp` (`no_hp`),
  CONSTRAINT `tbl_suratjalan_ibfk_1` FOREIGN KEY (`no_hp`) REFERENCES `tbl_hp` (`no_hp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_suratjalan
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_users`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `user_id` varchar(20) NOT NULL DEFAULT '',
  `password` char(32) DEFAULT NULL,
  `level` varchar(15) NOT NULL,
  `nama` varchar(15) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES ('001', 'fitri', 'admin', 'fitri');
INSERT INTO `tbl_users` VALUES ('a', 'a', 'customer', 'a');
INSERT INTO `tbl_users` VALUES ('b', 'b', 'admin', 'abc');

-- ----------------------------
-- Table structure for `tracking`
-- ----------------------------
DROP TABLE IF EXISTS `tracking`;
CREATE TABLE `tracking` (
  `nomor` int(5) NOT NULL AUTO_INCREMENT,
  `x` decimal(8,5) NOT NULL,
  `y` decimal(8,5) NOT NULL,
  `nama_tempat` varchar(100) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `no_po` varchar(15) NOT NULL,
  `status` varchar(11) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`nomor`),
  KEY `no_hp` (`no_hp`),
  CONSTRAINT `tracking_ibfk_1` FOREIGN KEY (`no_hp`) REFERENCES `tbl_hp` (`no_hp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tracking
-- ----------------------------
