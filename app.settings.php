<?php
#
#   SETTINGS
#
return array(
    'mode' => 'development', // 'production' atau 'development'
    'db' => array(
          'host'     => 'localhost',
          'user'     => 'root',
          'password' => '',
          'dbname'   => 'test',
          'driver'   => 'pdo_mysql',
    ),
    'exclude_auth' => array(
          'index.php',
          'login.php',
          'ajax.cek_user.php',
          'tracking/index.php',
    )
);
