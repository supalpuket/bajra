<?php

// setting aplikasi
include("app.init.php");

if (!isXhr()) {
    header("location:index.php");
    exit;
} else {
    $db     = connectdb(); //koneksi db
    $proses = $_GET['nama']; // nama proses
    //membuat variabel $hp dan datanya dari inputan no hp
    $hp     = $db->real_escape_string($_GET['no_hp']);
    //membuat variabel $nama dan datanya dari inputan Nama supir
    $nama   = $db->real_escape_string($_GET['nm_supir']);

    switch($proses) {

        case 'tambah': // tambah data hp
            //melakukan query dengan perintah INSERT INTO untuk memasukkan data ke database
            $input = $db->query("INSERT INTO tbl_hp VALUES('$hp','$nama')");
            //jika query input sukses
            if ($input) die(OK);
            // mysql error code: ER_DUP_ENTRY
            if (mysqlerror($db->errno, 'ER_DUP_ENTRY')) die("No HP sudah terdaftar");
            break;

        case 'edit': // edit hp
            $update = $db->query("UPDATE tbl_hp SET nm_supir='$nama' WHERE no_hp='$hp' ");
            // proses update berhasil
            if ($update) die(OK);
            break;

        case 'hapus': // hapus data hp
            //jika data ada di database, maka melakukan query DELETE table hp dengan kondisi WHERE hp_id='$id'
            $delete = $db->query("DELETE FROM tbl_hp WHERE no_hp='$hp' ");

            //jika query DELETE berhasil
            if ($delete) {
                die(OK);
            }

            if (mysqlerror($db->errno, 'ER_ROW_IS_REFERENCED_2')) {
                die("Beberapa data surat jalan mencantumkan no hp ini");
            }
    }

    $path = pathinfo(__FILE__);
    print_r($_DEV_MODE ? get_defined_vars() : $path['basename']);
}
