<?php

if (!isset($settings['db'])) {
    include("app.init.php");
}
// koneksi database
function connectdb() {
    $settings = include("app.settings.php");
    $db       = (object) $settings['db'];
    return new mysqli($db->host, $db->host, $db->password, $db->dbname);
}

// generator no suratjalan  "SJ1507-00001"
function no_suratjalan() {
    $db     = connectdb();
    $result = $db->query("SELECT MAX(RIGHT(no_suratjalan, 5)) AS _lastnum_ FROM tbl_suratjalan")
        or die(print_r($db->error_list));
    $result && ($data   = $result->fetch_object());
    $date   = date('ym');

    return sprintf("SJ{$date}-%05d", ++$data->_lastnum_);
}

/**
 *  @param $errno mysqli error number
 *  @param $constant mysqli error constant name e.g. 'ERR_DUP_ENTRY'
 *  @return boolean true if ERR_DUP_ENTRY == $errno otherwise false returned
 */
function mysqlerror($errno, $constant) {
    $file = "database/mysqld_error.txt"; // must be specified first

    foreach ((array) @file($file) as $line) {
        list (, $data_constant, $data_errno) = explode(" ", $line);

        if ($errno != $data_errno) continue;
        return ($constant == $data_constant);
    }

    return false;
}

function pagination_links($page, $total, $limit) {

    $range = 3;      // page lookahead/lookbehind range
    $start = $page - $range;
    $end   = ceil($total / $limit);

    $html = "<div class=\"$list_class\">";

    if ($start > 1) {
        $_GET['p'] = 1;
        $httpq     = http_build_query($_GET);
        $html .= "<a href=\"?$httpq\">[1]</a>";
    }

    if ($start > 2) $html .= '<span>...</span>';

    for ($i = $start; $i <= $end; $i++) {

        if ($i < 1) continue;
        if ($i > $page + $range) break;

        if ($page == $i) {
            $html .= "<span class=\"active\">[$i]</span>";
        } else {
            $_GET['p'] = $i;
            $httpq     = http_build_query($_GET);
            $html .= "<span><a href=\"?$httpq\">[$i]</a></span>";
        }
    }

    if ($i < $end) {
        $_GET['p'] = $end;
        $httpq     = http_build_query($_GET);
        $html .= "<span>...</span><span><a href=\"?$httpq\">[$end]</a></span>";
    }

    return $html;
}

// Is Ajax requests
function isXhr() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        ($_SERVER['HTTP_X_REQUESTED_WITH'] === "XMLHttpRequest");
}
