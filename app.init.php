<?php

################################################################################
#
#    Di bawah tidak  perlu diubah kecuali tau apa yang dilakukan.
#
$settings = require("app.settings.php");
require("app.functions.php");
error_reporting(0);
session_start();

# Folder instalasi
$_APP_DIR  = str_replace('\\', '/', __DIR__);
$_DEV_MODE = $settings['mode'] == 'development';

if ($_DEV_MODE) {
    ini_set('display_error', 1);
    error_reporting(E_ALL ^ E_NOTICE);
}

# koneksi database
$db = (object) $settings['db'];
mysql_connect($db->host, $db->host, $db->password);
mysql_select_db($db->dbname);

# file yang eksekusi saat ini
$page = str_replace(['\\', "$_APP_DIR/"], ['/', ''], $_SERVER['SCRIPT_FILENAME']);

# harus login dulu untuk mengakses page
# kecuali page yang ada di daftar 'exclude_auth'
if (!in_array($page, $settings['exclude_auth'])) {

    # cek sesi login
    if (!($_SESSION['login'])) {
        header("location:login.php");
        exit();
    }

    # customer ngga boleh buka page admin
    if ($_SESSION['login']['level'] == 'customer') {
        die("<script>alert('Hanya untuk admin');"
            . "location='index.php'</script>");
    }
}
