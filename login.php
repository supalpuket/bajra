<?php
include ("app.init.php");

if (!empty($_SESSION['login'])) {
    header("location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
        <style type="text/css">
            body{
                background:url(back.png);
                background:url(http://mymaplist.com/img/parallax/pinlayer2.png),url(http://mymaplist.com/img/parallax/pinlayer1.png),url(http://mymaplist.com/img/parallax/back.png);
            }

            .vertical-offset-100{
                padding-top:100px;
            }
        </style>

    </head>
    <body>

        <div class="container">
            <div class="row vertical-offset-100">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 align="center" class="panel-title"><span class="form-group"><img src="../logo.jpg" width="280" height="144"></span></h3>
                            <h1 align="center" class="panel-title"><strong>Silahkan Login</strong></h1>
                        </div>
                        <form id="login_form">
                            <div class="panel-body">
                                <div align="center">
                                    <p class="panel-title">
                                        <strong>
                                            <span class="form-group">
                                                <input class="form-control" placeholder="Username" name="username" required value="" id="txt_username"/>
                                            </span>
                                        </strong>
                                    </p>
                                    <p align="center" class="panel-title">
                                        <strong>
                                            <span class="form-group">
                                                <input class="form-control" placeholder="Password" name="password" type="password" value="" id="txt_password" required=""/>
                                            </span>
                                        </strong>
                                    </p>
                                </div>
                            </div>
                            <div align="center">
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Login" name="login" id="btn_login"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div align="center">
        </div>
    </body>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easyui.min.js"></script>
    <script src="js/app.login.js"></script>
</html>
